//
// ModuleInputTest.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 17:46:43 2014 citolen
// Last update Mon Jan  6 18:30:05 2014 citolen
//

#include "api/AInputModule.hh"
#include "api/HTTPTransaction.hh"

class	ModuleInputTest : public ZIAPI::AInputModule
{
public:
  ModuleInputTest();
  ~ModuleInputTest();

  void	load();
  void	loadConfiguration(const std::map<std::string, std::string>& configs);
  void	unload();

  void	inputEntryHook();
  void	send(ZIAPI::HTTPTransaction*);
};
