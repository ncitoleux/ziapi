//
// ModuleInputTest.cpp for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 17:48:37 2014 citolen
// Last update Tue Jan  7 09:47:12 2014 citolen
//

#include <cstdio>
#include <cstdlib>
#if defined(_WIN32)
#include <io.h>
#define read _read
#else
#include <unistd.h>
#endif
#include <iostream>
#include "ModuleInputTest.hh"

ModuleInputTest::ModuleInputTest()
  : AInputModule("Input Module Test")
{
}

ModuleInputTest::~ModuleInputTest()
{
}

void	ModuleInputTest::load()
{
  registerHook(ZIAPI::Hook::INPUT_ENTRY, NULL);
}

void	ModuleInputTest::loadConfiguration(const std::map<std::string, std::string>&)
{
}

void	ModuleInputTest::unload()
{
}

void	ModuleInputTest::inputEntryHook()
{
  int	r;
  char	*buf;
  ZIAPI::HTTPConnection *connection = new ZIAPI::HTTPConnection(NULL);

  buf = (char*)malloc(256);
  while ((r = read(0, buf, 256)) > 0)
    {
      connection->setBuffer(new std::string(buf, r));
      notifyRead(connection);
    }
}

void	ModuleInputTest::send(ZIAPI::HTTPTransaction*)
{
  std::cout << "Transaction send" << std::endl;
}
