var searchData=
[
  ['httpconnection',['HTTPConnection',['../class_z_i_a_p_i_1_1_h_t_t_p_connection.html#a56d2d3126d78ea3171835fe0acc57a3e',1,'ZIAPI::HTTPConnection']]],
  ['httprequest',['HTTPRequest',['../class_z_i_a_p_i_1_1_h_t_t_p_request.html#abfeeda4c33c00aaaafdb93cbb6387cff',1,'ZIAPI::HTTPRequest::HTTPRequest()'],['../class_z_i_a_p_i_1_1_h_t_t_p_request.html#afc32914c444f12d6dae831b0b3126aa3',1,'ZIAPI::HTTPRequest::HTTPRequest(HTTPMethod method, const std::string &amp;uri)']]],
  ['httpresponse',['HTTPResponse',['../class_z_i_a_p_i_1_1_h_t_t_p_response.html#a0fe0cec3c63a67672167f8b3b8044bc3',1,'ZIAPI::HTTPResponse']]],
  ['httptransaction',['HTTPTransaction',['../class_z_i_a_p_i_1_1_h_t_t_p_transaction.html#aeebfa7e33a634d251bd044892ace7fa8',1,'ZIAPI::HTTPTransaction']]]
];
