//
// ModuleTest.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 20:30:00 2014 citolen
// Last update Tue Jan  7 10:22:28 2014 citolen
//

#ifndef __MODULETEST_HH__
#define __MODULETEST_HH__

#include "api/AModule.hh"
#include "api/HTTPTransaction.hh"

class ModuleTest : public ZIAPI::AModule
{
public:
  ModuleTest();
  ~ModuleTest();

  void	load();
  void	loadConfiguration(const std::map<std::string, std::string>& configs);
  void	unload();

  void	regHook(ZIAPI::AModule*,ZIAPI::Hook::HookEnum,ZIAPI::HookResult::HookResultEnum (*)(ZIAPI::HTTPTransaction*));

  void	configKey(ZIAPI::AModule*, const std::string&);

  void	configFile(ZIAPI::AModule*, const std::string&);

  static ZIAPI::HookResult::HookResultEnum	logTest(ZIAPI::HTTPTransaction*);
};

#endif
