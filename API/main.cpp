//
// main.cpp for main in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 18:00:29 2014 citolen
// Last update Tue Jan  7 14:17:11 2014 citolen
//

#include <iostream>

#include "api/AInputModule.hh"
#include "api/AModule.hh"
#include "api/Callback.hpp"
#include "ModuleInputTest.hh"


#if defined(_WIN32)
#include <Windows.h>
#else
#include <dlfcn.h>
#endif


void	processHook(ZIAPI::AModule *module,
		    ZIAPI::Hook::HookEnum hookPoint,
		    ZIAPI::HookResult::HookResultEnum (*)(ZIAPI::HTTPTransaction*))
{
  if (hookPoint == ZIAPI::Hook::INPUT_ENTRY)
    {
      ZIAPI::AInputModule *inputModule;
      if ((inputModule = dynamic_cast<ZIAPI::AInputModule*>(module)))
		inputModule->inputEntryHook();
    }
}

void	processConfigKey(ZIAPI::AModule *,
			 const std::string& configKey)
{
  std::cout << "configKey:" << configKey << std::endl;
}

void	processConfigFile(ZIAPI::AModule *,
			  const std::string& configFile)
{
  std::cout << "configFile:" << configFile << std::endl;
}

void	callbackRead(ZIAPI::AInputModule *module, ZIAPI::HTTPConnection *connection)
{
  std::cout << "Callback Read" << std::endl;
  module->send(connection->getHTTPTransaction());
}

int	main()
{
  ZIAPI::HTTPRequest request(ZIAPI::HTTPRequest::POST, "/");

  ModuleInputTest inputTest;

  HOOK_REGISTER(&inputTest, &processHook);
  CONFIG_REGISTER(&inputTest, &processConfigKey);
  CONFIG_FILE_REGISTER(&inputTest, &processConfigFile);
  INPUT_MODULE_SET_STATIC_CALLBACK(&inputTest, &callbackRead);

  std::cout << "Input module test. Type something:" << std::endl;
  inputTest.load();

  std::cout << std::endl << "Dynamicly loading a module, test:" << std::endl;
  ZIAPI::AModule* module = NULL;
  ZIAPI::AModule	*(*fcn)();

#if defined(_WIN32)
  HMODULE handle = LoadLibrary("ModuleTest.dll");
  if (handle != NULL)
  {
	  fcn = (ZIAPI::AModule*(*)())GetProcAddress(handle, "createModule");
	  if (fcn != NULL)
		  module = fcn();
  }
#else
  void	*handle;
  handle = dlopen("./moduleTest.so", RTLD_NOW);
  if (handle)
	{
      fcn = reinterpret_cast<ZIAPI::AModule *(*)()>(dlsym(handle, "createModule"));
      if (fcn)
		module = fcn();
	}
#endif

  if (module != NULL)
	{
		HOOK_REGISTER(module, &processHook);
		CONFIG_REGISTER(module, &processConfigKey);
		CONFIG_FILE_REGISTER(module, &processConfigFile);
		std::cout << "Module name loaded: " << module->getName() << std::endl;
		std::cout << "Simulate load:" << std::endl;
		module->load();
	}

#if defined(_WIN32)
	if (handle != NULL)
	{
		FreeLibrary(handle);
	}
#else
  if (handler != NULL)
	{
		dlclose(handle);
	}
#endif
      
	std::cin.get();

  return (0);
}
