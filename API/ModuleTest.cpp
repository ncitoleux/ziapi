//
// ModuleTest.cpp for ModuleTest in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 20:31:52 2014 citolen
// Last update Tue Jan  7 10:22:48 2014 citolen
//

#include <iostream>
#include "ModuleTest.hh"

ModuleTest::ModuleTest()
  : AModule("ModuleTest")
{

}

ModuleTest::~ModuleTest()
{

}

ZIAPI::HookResult::HookResultEnum	ModuleTest::logTest(ZIAPI::HTTPTransaction*)
{
  std::cout << "Log::Test:| test de log !" << std::endl;
  return (ZIAPI::HookResult::OK);
}

void	ModuleTest::load()
{
  registerConfig("max-threads");
  registerConfigFile("httpd.conf");
  registerHook(ZIAPI::Hook::LOG, &ModuleTest::logTest);
}

void	ModuleTest::loadConfiguration(const std::map<std::string, std::string>&)
{

}

void	ModuleTest::unload()
{

}

void	ModuleTest::regHook(ZIAPI::AModule*, ZIAPI::Hook::HookEnum, ZIAPI::HookResult::HookResultEnum (*)(ZIAPI::HTTPTransaction *httpTransaction))
{
  std::cout << "bonjour" << std::endl;
}

void	ModuleTest::configKey(ZIAPI::AModule*, const std::string&)
{
  std::cout << "configKey" << std::endl;
}

void	ModuleTest::configFile(ZIAPI::AModule*, const std::string&)
{
  std::cout << "configFile" << std::endl;
}

extern "C"
{
#if defined(_WIN32)
	__declspec(dllexport)
#endif
  ZIAPI::AModule*	createModule()
  {
    return (new ModuleTest());
  }
}
