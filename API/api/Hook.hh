//
// Hook.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Sun Jan  5 22:06:20 2014 citolen
// Last update Tue Jan  7 10:25:59 2014 citolen
//

namespace	ZIAPI
{
  class Hook
  {
  public:
    ///
    /// \enum Hook point
    /// \brief Hook enumeration, HTTP processing step
    ///
    enum	HookEnum
      {
	INPUT_ENTRY, ///< Connection establishment
	COMMAND_LINE_COMPLETED, ///< First line parsed
	HEADER_COMPLETED, ///< Header parsed
	BODY_COMPLETED, ///< Body parsed
	URI_RESOLUTION, ///< URI resolution (uri rewrite)
	ACCESS_CHECK, ///< Check access permission
	GENERATE_RESPONSE, ///< Make response
	LOG ///< Log
      };
  };

  class HookResult
  {
  public:
    ///
    /// \enum Hook possible result
    /// \brief HookResult enumeration
    ///
    enum	HookResultEnum
      {
	OK, ///< Ok
	DECLINED /// Declined
      };
  };
}
