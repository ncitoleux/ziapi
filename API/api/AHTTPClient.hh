//
// HTTPClient.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 13:13:55 2014 citolen
// Last update Tue Jan  7 10:27:59 2014 citolen
//

#ifndef __AHTTPCLIENT_HH__
#define __AHTTPCLIENT_HH__

#include <string>

namespace	ZIAPI
{
  class	AHTTPClient;
}

#include "AModule.hh"

namespace	ZIAPI
{

  ///
  /// \brief Abstract representation of an HTTP client
  ///
  class	AHTTPClient
  {
    AModule	*_inputModule;

  public:
    ///
    /// \brief Constructor
    /// \param inputModule Module that handle the client
    ///
    AHTTPClient(AModule *inputModule);

    ///
    /// \brief Destructor
    ///
    ~AHTTPClient();

    ///
    /// \brief Return the input module that create this instance
    /// \return InputModule
    ///
    AModule*			getInputModule() const;

    ///
    /// \brief Return the IP address of the client
    /// \Return IP address
    ///
    virtual const std::string&	getIP() const = 0;

    ///
    /// \brief Return the port used by the client
    /// \Return Client port
    ///
    virtual int			getPort() const = 0;
  };
}

#endif
