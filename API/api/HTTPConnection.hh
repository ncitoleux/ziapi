//
// HTTPConnection.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 17:11:25 2014 citolen
// Last update Mon Jan  6 18:23:32 2014 citolen
//

#ifndef __HTTPCONNECTION_HH__
#define __HTTPCONNECTION_HH__

#include <string>
#include "HTTPTransaction.hh"

namespace	ZIAPI
{
  ///
  /// \brief Represent an HTTP Connection
  /// One for each client, use to communicate with the core
  ///
  class	HTTPConnection
  {
    HTTPTransaction	*_httpTransaction;
    std::string		*_buffer;
    std::string		*_restBuffer;

  public:
    ///
    /// \brief Constructor
    /// \param httpClient HTTP Client
    ///
    HTTPConnection(AHTTPClient *httpClient);

    ///
    /// \brief Destructor
    ///
    ~HTTPConnection();

    ///
    /// \brief Return the HTTP Transaction
    /// \return HTTP Transaction
    ///
    HTTPTransaction*	getHTTPTransaction() const;

    ///
    /// \brief Return the Current Buffer
    /// \return Last octet read
    ///
    std::string*	getBuffer() const;

    ///
    /// \brief Return the rest of the buffer in case the core saved some
    /// \return The rest of the buffer
    ///
    std::string*	getRestBuffer() const;

    ///
    /// \brief Set the HTTP Transaction to the new value
    /// \param httpTransaction HTTP Transaction new value
    ///
    void		setHTTPTransaction(HTTPTransaction *httpTransaction);

    ///
    /// \brief Set the Buffer to the new value
    /// \param buffer Buffer new value
    ///
    void		setBuffer(std::string *buffer);

    ///
    /// \brief Set the Rest Buffer to the value
    /// \param restBuffer Rest buffer new value
    ///
    void		setRestBuffer(std::string *restBuffer);
  };
}

#endif
