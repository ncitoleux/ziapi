//
// AModule.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 20:10:06 2014 citolen
// Last update Tue Jan  7 10:26:11 2014 citolen
//

#ifndef __AMODULE_HH__
#define __AMODULE_HH__

#if defined(_WIN32)
#define DECLDIR __declspec(dllexport)
#endif

#include <string>
#include <map>

#include "Callback.hpp"
#include "Hook.hh"
#include "Macro.hh"

namespace	ZIAPI
{
  class	AModule;
}

#include "HTTPTransaction.hh"

namespace	ZIAPI
{

  typedef HookResult::HookResultEnum (*HookSignature)(HTTPTransaction*);

  ///
  /// \brief Base of a module
  ///
  class	AModule
  {
    std::string	_name; /*!< Name of the Module */

  public:
    ///
    /// \brief Module constructor
    /// \param name Module name
    ///
    AModule(const std::string& name);

    ///
    /// \brief Destructor
    ///
    virtual ~AModule();

    ///
    /// \brief Return name of the module
    /// \return Module name
    ///
    const std::string& getName() const;

    ///
    /// \brief Module load function
    /// Call when the core wants to load the module
    ///
    virtual void load() = 0;

    ///
    /// \brief Module load configuration function
    /// \param configs Configurations
    /// Call when the core wants to load/reload the configuration
    /// configs contains the key/value for the registered key (registered with registerConfig function)
    ///
    virtual void loadConfiguration(const std::map<std::string, std::string>& configs) = 0;

    ///
    /// \brief Module unload function
    /// Call when the core wants to unload the module
    ///
    virtual void unload() = 0;

    Callback<AModule*, Hook::HookEnum, HookResult::HookResultEnum (*)(HTTPTransaction*)>		*_registerHookFunction; /*!< Register Hook callback, to link with your core*/
    Callback<AModule*, const std::string&>		*_registerConfigFunction; /*!< Register Config callback, to link with your core */
    Callback<AModule*, const std::string&>		*_registerConfigFileFunction; /*!< Register Config file callback, to link with your core */

  protected:
    ///
    /// \brief Register an Hook
    ///
    void			registerHook(Hook::HookEnum hookPoint, HookResult::HookResultEnum (*)(HTTPTransaction*));

    ///
    /// \brief Register an config key
    ///
    void			registerConfig(const std::string& configKey);

    ///
    /// \brief Register an config file
    ///
    void			registerConfigFile(const std::string& configFile);
  };
}

#endif
