//
// HTTPClient.cpp for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 15:44:15 2014 citolen
// Last update Mon Jan  6 22:09:00 2014 citolen
//

#include "AHTTPClient.hh"

ZIAPI::AHTTPClient::AHTTPClient(AModule *inputModule)
  : _inputModule(inputModule)
{
}

ZIAPI::AHTTPClient::~AHTTPClient()
{
}

ZIAPI::AModule*		ZIAPI::AHTTPClient::getInputModule() const
{
  return (_inputModule);
}
