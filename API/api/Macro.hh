//
// Macro.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 11:38:08 2014 citolen
// Last update Tue Jan  7 14:17:04 2014 citolen
//

///
/// Set AModule::registerHookFunction to X
///
#ifndef HOOK_REGISTER
#define HOOK_REGISTER(MODULE, FUNC)	(MODULE)->_registerHookFunction = new ZIAPI::CallbackS<ZIAPI::AModule*, ZIAPI::Hook::HookEnum, ZIAPI::HookResult::HookResultEnum (*)(ZIAPI::HTTPTransaction*)>(FUNC);
#endif

///
/// Set AModule::registerHookFunction to the member function X from class C
///
#ifndef HOOK_REGISTER_CLASS
#define HOOK_REGISTER_CLASS(MODULE, CLASSNAME, CLASS, FUNC) (MODULE)->_registerHookFunction = new ZIAPI::CallbackM<CLASSNAME, ZIAPI::AModule*, ZIAPI::Hook::HookEnum, ZIAPI::HookResult::HookResultEnum (*)(ZIAPI::HTTPTransaction*)>(CLASS, FUNC);
#endif


///
/// Set AModule::registerConfigFunction to X
///
#ifndef CONFIG_REGISTER
#define CONFIG_REGISTER(MODULE, FUNC)	(MODULE)->_registerConfigFunction = new ZIAPI::CallbackS<ZIAPI::AModule*, const std::string&>(FUNC);
#endif

///
/// Set AModule::registerHookFunction to the member function X from class C
///
#ifndef CONFIG_REGISTER_CLASS
#define CONFIG_REGISTER_CLASS(MODULE, CLASSNAME, CLASS, FUNC) (I)->_registerConfigFunction = new ZIAPI::CallbackM<CLASSNAME, ZIAPI::AModule*, const std::string&>(CLASS, FUNC);
#endif

///
///
///
#ifndef CONFIG_FILE_REGISTER
#define CONFIG_FILE_REGISTER(MODULE, FUNC)	(MODULE)->_registerConfigFileFunction = new ZIAPI::CallbackS<ZIAPI::AModule*, const std::string&>(FUNC);
#endif

///
///
///
#ifndef CONFIG_FILE_REGISTER_CLASS
#define CONFIG_FILE_REGISTER_CLASS(MODULE, CLASSNAME, CLASS, FUNC)	(MODULE)->_registerConfigFileFunction = new ZIAPI::CallbackM<CLASSNAME, ZIAPI::AModule*, const std::string&>(CLASS, FUNC);
#endif

///
///
///
#ifndef INPUT_MODULE_SET_STATIC_CALLBACK
#define INPUT_MODULE_SET_STATIC_CALLBACK(MODULE, FUNC) (MODULE)->setCallbackRead(new ZIAPI::CallbackS<ZIAPI::AInputModule*, ZIAPI::HTTPConnection*>(FUNC));
#endif

///
///
///
#ifndef INPUT_MODULE_SET_MEMBER_CALLBACK
#define INPUT_MODULE_SET_MEMBER_CALLBACK(MODULE, CLASSNAME, CLASS, FUNC) (MODULE)->setCallbackRead(new ZIAPI::CallbackM<CLASSNAME, ZIAPI::AInputModule*, ZIAPI::HTTPConnection*>(CLASS, FUNC));
#endif
