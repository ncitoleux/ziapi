//
// AModule.cpp for AModule in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 20:15:52 2014 citolen
// Last update Tue Jan  7 10:20:57 2014 citolen
//

#include <iostream>
#include "Callback.hpp"
#include "AModule.hh"

ZIAPI::AModule::AModule(const std::string& name)
  : _name(name),
	_registerHookFunction(NULL),
	_registerConfigFunction(NULL),
	_registerConfigFileFunction(NULL)
{}

ZIAPI::AModule::~AModule()
{}

const std::string&	ZIAPI::AModule::getName() const
{
  return (_name);
}

void	ZIAPI::AModule::registerHook(Hook::HookEnum hookPoint, HookResult::HookResultEnum (*hookCallback)(HTTPTransaction*))
{
  if (_registerHookFunction != NULL)
    _registerHookFunction->invoke(this, hookPoint, hookCallback);
}

void	ZIAPI::AModule::registerConfig(const std::string& configKey)
{
  if (_registerConfigFunction != NULL)
    _registerConfigFunction->invoke(this, configKey);
}

void	ZIAPI::AModule::registerConfigFile(const std::string& configFile)
{
  if (_registerConfigFileFunction != NULL)
    _registerConfigFileFunction->invoke(this, configFile);
}
