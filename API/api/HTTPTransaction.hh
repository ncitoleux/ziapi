//
// HTTPTransaction.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 15:59:05 2014 citolen
// Last update Mon Jan  6 21:58:00 2014 citolen
//

#ifndef __HTTPTRANSACTION_HH__
#define __HTTPTRANSACTION_HH__

#include "HTTPRequest.hh"
#include "HTTPResponse.hh"

namespace ZIAPI
{
  class HTTPTransaction;
}

#include "AHTTPClient.hh"

namespace ZIAPI
{

  ///
  /// \brief Represent a container of the necessary objects to process an HTTP request
  ///
  class	HTTPTransaction
  {
    HTTPRequest		*_httpRequest;
    HTTPResponse	*_httpResponse;
    AHTTPClient		*_httpClient;

  public:
    ///
    /// \brief Constructor
    /// \param httpRequest HTTP Request object
    /// \param httpResponse HTTP Response object
    /// \param httpClient HTTP Client object
    ///
    HTTPTransaction(HTTPRequest *httpRequest = NULL,
		    HTTPResponse *httpResponse = NULL,
		    AHTTPClient *httpClient = NULL);

    ///
    /// \brief Destructor
    ///
    ~HTTPTransaction();

    ///
    /// \brief Return the HTTP Request object
    /// \Return HTTP Request
    ///
    HTTPRequest*	getHTTPRequest() const;

    ///
    /// \brief Return the HTTP Response object
    /// \Return HTTP Response
    ///
    HTTPResponse*	getHTTPResponse() const;

    ///
    /// \brief Return the HTTP Client object
    /// \Return HTTP Client
    ///
    AHTTPClient*	getHTTPClient() const;

    ///
    /// \brief Set the HTTP Request with the new value
    /// \param httpRequest HTTP Request new value
    ///
    void		setHTTPRequest(HTTPRequest* httpRequest);

    ///
    /// \brief Set the HTTP Response with the new value
    /// \param httpResponse HTTP Response new value
    ///
    void		setHTTPResponse(HTTPResponse* httpResponse);

    ///
    /// \brief Set the HTTP Client with the new value
    /// \param httpClient HTTP Client new value
    ///
    void		setHTTPClient(AHTTPClient* httpClient);
  };
}

#endif
