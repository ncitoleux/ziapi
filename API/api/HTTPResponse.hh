//
// HTTPResponse.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 19:27:29 2014 citolen
// Last update Mon Jan  6 10:15:47 2014 citolen
//

#ifndef __HTTPRESPONSE_HH__
#define __HTTPRESPONSE_HH__

#include <string>
#include <map>
#include "HTTPVersion.hh"

namespace ZIAPI
{
  ///
  /// \class HTTPResponse
  /// \brief Represent an HTTP response
  ///
  class	HTTPResponse
  {
    int					_code;    /*!< HTTP response code */
    HTTPVersion				_version; /*!< HTTP version */
    std::map<std::string, std::string>	_fields;  /*!< HTTP header fields */
    std::string				_body;    /*!< HTTP body */

  public:
    ///
    /// \brief Constructor
    ///
    HTTPResponse();

    ///
    /// \brief Destructor
    ///
    ~HTTPResponse();

    ///
    /// \brief Return the HTTP reponse code value
    /// \return HTTP response code
    ///
    int			getCode() const;

    ///
    /// \param code HTTP response code
    /// \brief Set the HTTP reponse code value
    ///
    void		setCode(int code);

    ///
    /// \brief Return HTTP version
    /// \return HTTP version
    ///
    const HTTPVersion&	getVersion() const;

    ///
    /// \param version HTTP version
    /// \brief Set HTTP version
    ///
    void		setVersion(HTTPVersion version);

    ///
    /// \brief Return all the fields of the HTTP request
    /// \return Map containing all the fields of the HTTP request header
    ///
    const std::map<std::string, std::string>&	getFields() const;

    ///
    /// \brief Return the value of a field if the key exists otherwise throw an exception
    /// \param fieldName Field name
    /// \return Value of the field with the given key
    ///
    const std::string&	getField(const std::string& fieldName) const;

    ///
    /// \brief Set HTTP version
    /// \param fieldName Field name
    /// \param value Field value
    ///
    void		setField(const std::string& fieldName, const std::string& value);

    ///
    /// \brief Return HTTP body
    /// \return HTTP body
    ///
    const std::string&	getBody() const;

    ///
    /// \brief Set HTTP body
    /// \param body HTTP body
    ///
    void		setBody(const std::string& body);
  };
}

#endif
