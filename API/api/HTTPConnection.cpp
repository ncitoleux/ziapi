//
// HTTPConnection.cpp for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 17:26:38 2014 citolen
// Last update Mon Jan  6 17:44:33 2014 citolen
//

#include "HTTPConnection.hh"

ZIAPI::HTTPConnection::HTTPConnection(AHTTPClient *httpClient)
{
  _httpTransaction = new HTTPTransaction(NULL, NULL, httpClient);
  _buffer = NULL;
  _restBuffer = NULL;
}

ZIAPI::HTTPConnection::~HTTPConnection()
{
}

ZIAPI::HTTPTransaction*	ZIAPI::HTTPConnection::getHTTPTransaction() const
{
  return (_httpTransaction);
}

std::string*		ZIAPI::HTTPConnection::getBuffer() const
{
  return (_buffer);
}

std::string*		ZIAPI::HTTPConnection::getRestBuffer() const
{
  return (_restBuffer);
}

void			ZIAPI::HTTPConnection::setHTTPTransaction(HTTPTransaction *httpTransaction)
{
  _httpTransaction = httpTransaction;
}

void			ZIAPI::HTTPConnection::setBuffer(std::string *buffer)
{
  _buffer = buffer;
}

void			ZIAPI::HTTPConnection::setRestBuffer(std::string *restBuffer)
{
  _restBuffer = restBuffer;
}
