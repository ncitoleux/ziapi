//
// AInputModule.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 15:14:22 2014 citolen
// Last update Mon Jan  6 22:11:23 2014 citolen
//

#ifndef __AINPUTMODULE_HH__
#define __AINPUTMODULE_HH__

#include "AModule.hh"
#include "HTTPConnection.hh"

namespace	ZIAPI
{
  ///
  /// \brief Represent an input module
  ///
  class	AInputModule : public AModule
  {
    ZIAPI::Callback<AInputModule*, HTTPConnection*>	*_callbackRead;

  protected:
    ///
    /// \brief notify the core that something's going on
    /// \param packet HTTP connection
    ///
    void			notifyRead(HTTPConnection *connection);

  public:
    ///
    /// \brief Constructor
    /// \param name Module name
    ///
    AInputModule(const std::string &name);

    ///
    /// \brief Destructor
    ///
    ~AInputModule();

    ///
    /// \brief Set the callback
    /// Used by ::notifyRead() to link the core
    ///
    void			setCallbackRead(ZIAPI::Callback<AInputModule*, HTTPConnection*>*);

    ///
    /// \brief Send
    /// Used by the core to tell the module input that the HTTPTransaction should be send
    ///
    virtual void		send(HTTPTransaction*) = 0;

    ///
    /// \brief Input entry hook
    /// Called when registered to the Hook INPUT_ENTRY
    ///
    virtual void		inputEntryHook() = 0;
  };
}

#endif
