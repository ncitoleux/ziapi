//
// HTTPResponse.cpp for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 19:42:06 2014 citolen
// Last update Sun Jan  5 21:49:38 2014 citolen
//

#include "HTTPResponse.hh"

ZIAPI::HTTPResponse::HTTPResponse()
{}

ZIAPI::HTTPResponse::~HTTPResponse()
{}

int				ZIAPI::HTTPResponse::getCode() const
{
  return (_code);
}

void				ZIAPI::HTTPResponse::setCode(int code)
{
  _code = code;
}

const ZIAPI::HTTPVersion&	ZIAPI::HTTPResponse::getVersion() const
{
  return (_version);
}

void				ZIAPI::HTTPResponse::setVersion(HTTPVersion version)
{
  _version = version;
}

const std::map<std::string, std::string>&	ZIAPI::HTTPResponse::getFields() const
{
  return (_fields);
}

const std::string&	ZIAPI::HTTPResponse::getField(const std::string& fieldName) const
{
  if (_fields.count(fieldName) == 1)
    return (_fields.find(fieldName)->second);
  throw new std::exception();
}

void				ZIAPI::HTTPResponse::setField(const std::string& fieldName,
							      const std::string& value)
{
  _fields[fieldName] = value;
}

const std::string&		ZIAPI::HTTPResponse::getBody() const
{
  return (_body);
}

void				ZIAPI::HTTPResponse::setBody(const std::string& body)
{
  _body = body;
}
