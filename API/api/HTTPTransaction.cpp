//
// HTTPTransaction.cpp for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 16:06:15 2014 citolen
// Last update Mon Jan  6 16:11:17 2014 citolen
//

#include "HTTPTransaction.hh"

ZIAPI::HTTPTransaction::HTTPTransaction(HTTPRequest *httpRequest,
					HTTPResponse *httpResponse,
					AHTTPClient *httpClient)
  : _httpRequest(httpRequest),
    _httpResponse(httpResponse),
    _httpClient(httpClient)
{

}

ZIAPI::HTTPTransaction::~HTTPTransaction()
{
}

ZIAPI::HTTPRequest*	ZIAPI::HTTPTransaction::getHTTPRequest() const
{
  return (_httpRequest);
}

ZIAPI::HTTPResponse*	ZIAPI::HTTPTransaction::getHTTPResponse() const
{
  return (_httpResponse);
}

ZIAPI::AHTTPClient*	ZIAPI::HTTPTransaction::getHTTPClient() const
{
  return (_httpClient);
}

void		ZIAPI::HTTPTransaction::setHTTPRequest(HTTPRequest *httpRequest)
{
  _httpRequest = httpRequest;
}

void		ZIAPI::HTTPTransaction::setHTTPResponse(HTTPResponse *httpResponse)
{
  _httpResponse = httpResponse;
}

void		ZIAPI::HTTPTransaction::setHTTPClient(AHTTPClient *httpClient)
{
  _httpClient = httpClient;
}
