//
// Callback.hh for Callback in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 20:43:29 2014 citolen
// Last update Mon Jan  6 11:03:48 2014 citolen
//

#ifndef __CALLBACK_HH__
#define __CALLBACK_HH__

#include <cstdlib>

namespace ZIAPI
{
  ///
  /// \brief Abstract representation of an callback
  /// \n Template parameters 'T' represents the type parameters of the callback function.
  ///
  template<typename... T>
  class	Callback
  {
  public:
    ///
    /// \brief Constructor
    ///
    Callback() {}

    ///
    /// \brief Destructor
    ///
    virtual ~Callback() {}

    virtual void invoke(T... args) = 0;
  };

  ///
  /// \brief Callback for static function
  /// \n Template parameters 'T' represents the type parameters of the callback function.
  ///
  template<typename... T>
  class CallbackS : public Callback<T...>
  {
    void	(*_function)(T...);

  public:
    ///
    /// \brief Constructor
    /// \param function Static function
    ///
    CallbackS(void (*function)(T...))
    : _function(function)
    {}

    ///
    /// \brief Destructor
    ///
    ~CallbackS() {}

    void	invoke(T... args)
    {
      if (_function != NULL)
	_function(args...);
    }
  };

  ///
  /// \brief Callback for class member function
  /// \n Template parameters 'C' represents the class type of the callback function.
  /// \n Template parameters 'T' represents the type parameters of the callback function.
  ///
  template<typename C, typename... T>
  class CallbackM : public Callback<T...>
  {
    C		*_classObject;
    void	(C::*_function)(T...);

  public:
    ///
    /// \brief Constructor
    /// \param classObject Instance of the Class C
    /// \param function Function from the Class C
    ///
    CallbackM(C *classObject, void (C::*function)(T...))
      : _classObject(classObject), _function(function)
    {
    }
    ~CallbackM() {}

    void	setClassObject(C *obj)
    {
      _classObject = obj;
    }

    void	invoke(T... args)
    {
      if (_classObject != NULL)
  	(_classObject->*_function)(args...);
    }
  };
}

#endif
