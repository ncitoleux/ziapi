//
// HTTPRequest.hh for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 17:27:08 2014 citolen
// Last update Sun Jan  5 23:25:55 2014 citolen
//

#ifndef __HTTPREQUEST_HH__
#define __HTTPREQUEST_HH__

#include <string>
#include <map>

#include "HTTPVersion.hh"

namespace ZIAPI
{
  ///
  /// \brief Represent an HTTP request
  ///
  class	HTTPRequest
  {
  public:
    enum HTTPMethod
      {
	HEAD, ///< Head method
	GET,
	POST,
	PUT,
	DELETE,
	CONNECT,
	TRACE,
	OPTIONS
      };

  private:
    HTTPVersion				_version; /*!< HTTP version      */
    HTTPMethod				_method;  /*!< HTTP method       */
    std::string				_uri;     /*!< HTTP URI          */
    std::map<std::string, std::string>	_fields;  /*!< HTTP header field */
    std::string				_body;    /*!< HTTP body         */

  public:
    ///
    /// \brief Default constructor
    ///
    HTTPRequest();

    ///
    /// \brief Constructor
    ///
    HTTPRequest(HTTPMethod method, const std::string& uri);

    ///
    /// \brief Destructor
    ///
    ~HTTPRequest();

    ///
    /// \brief Return the version of the HTTP request
    /// \return Version of the HTTP request
    ///
    const HTTPVersion&				getVersion() const;

    ///
    /// \brief Return the method of the HTTPRequest
    /// \return Method of the HTTP request
    ///
    HTTPMethod					getMethod() const;

    ///
    /// \brief Return the URI of the HTTPRequest
    /// \return URI of the HTTP request
    ///
    const std::string&				getURI() const;

    ///
    /// \brief Return all the fields of the HTTP request
    /// \return Map containing all the fields of the HTTP request header
    ///
    const std::map<std::string, std::string>&	getFields() const;

    ///
    /// \brief Return the value of a field if the key exists otherwise throw an exception
    /// \param fieldName Field name
    /// \return Value of the field with the given key
    ///
    const std::string&				getField(const std::string& fieldName) const;

    ///
    /// \brief Check whether or not a field with the given key exists
    /// \param fieldName Field name
    /// \return True if the field exists, false otherwise
    ///
    bool					fieldExists(const std::string& fieldName) const;

    ///
    /// \brief Return the body of the HTTP request
    /// \return Body value
    ///
    const std::string&				getBody() const;

    ///
    /// \brief Set the version with the new value
    /// \param version new Version value
    ///
    void					setVersion(const HTTPVersion& version);

    ///
    /// \brief Set the uri with the new value
    /// \param uri new URI value
    ///
    void					setURI(const std::string& uri);

    ///
    /// \brief Set value to a field with the given key
    /// \param fieldName Field name
    /// \parem value New value
    ///
    void					setField(const std::string& fieldName,
							 const std::string& value);

    ///
    /// \brief Set body value
    /// \param body body value
    ///
    void					setBody(const std::string& body);
  };
}

std::ostream	&operator<<(std::ostream&, const ZIAPI::HTTPVersion&);
std::ostream	&operator<<(std::ostream&, const ZIAPI::HTTPRequest&);

#endif
