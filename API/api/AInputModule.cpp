//
// AInputModule.cpp for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Mon Jan  6 15:29:00 2014 citolen
// Last update Mon Jan  6 18:05:26 2014 citolen
//

#include "AInputModule.hh"

ZIAPI::AInputModule::AInputModule(const std::string &name)
  : AModule(name), _callbackRead(NULL)
{
}

ZIAPI::AInputModule::~AInputModule()
{
}

void	ZIAPI::AInputModule::notifyRead(HTTPConnection *connection)
{
  if (_callbackRead != NULL)
    _callbackRead->invoke(this, connection);
}

void	ZIAPI::AInputModule::setCallbackRead(ZIAPI::Callback<AInputModule*, HTTPConnection*> *callbackRead)
{
  _callbackRead = callbackRead;
}
