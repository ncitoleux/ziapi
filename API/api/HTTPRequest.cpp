//
// HTTPRequest.cpp for HTTPdAPI in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 17:43:47 2014 citolen
// Last update Sun Jan  5 21:49:09 2014 citolen
//

#include <exception>
#include <iostream>

#include "HTTPRequest.hh"

ZIAPI::HTTPRequest::HTTPRequest()
{
  _version.major = 1;
  _version.minor = 1;
  _method = HTTPRequest::GET;
}

ZIAPI::HTTPRequest::HTTPRequest(HTTPMethod method, const std::string& uri)
{
  _version.major = 1;
  _version.minor = 1;
  _method = method;
  _uri = uri;
}

ZIAPI::HTTPRequest::~HTTPRequest() {}

const ZIAPI::HTTPVersion&	ZIAPI::HTTPRequest::getVersion() const
{
  return (_version);
}

ZIAPI::HTTPRequest::HTTPMethod	ZIAPI::HTTPRequest::getMethod() const
{
  return (_method);
}

const std::string&		ZIAPI::HTTPRequest::getURI() const
{
  return (_uri);
}

const std::map<std::string, std::string>&	ZIAPI::HTTPRequest::getFields() const
{
  return (_fields);
}

const std::string&	ZIAPI::HTTPRequest::getField(const std::string& fieldName) const
{
  if (_fields.count(fieldName) == 1)
    return (_fields.find(fieldName)->second);
  throw new std::exception();
}

bool			ZIAPI::HTTPRequest::fieldExists(const std::string& fieldName) const
{
  if (_fields.count(fieldName) == 1)
    return (true);
  return (false);
}

const std::string&	ZIAPI::HTTPRequest::getBody() const
{
  return (_body);
}

void			ZIAPI::HTTPRequest::setVersion(const ZIAPI::HTTPVersion& version)
{
  _version = version;
}

void			ZIAPI::HTTPRequest::setURI(const std::string& uri)
{
  _uri = uri;
}

void			ZIAPI::HTTPRequest::setField(const std::string& fieldName,
						     const std::string& value)
{
  _fields[fieldName] = value;
}

void			ZIAPI::HTTPRequest::setBody(const std::string& body)
{
  _body = body;
}

///
/// Stream overload
///
std::ostream	&operator<<(std::ostream &os, const ZIAPI::HTTPVersion &version)
{
  os << version.major << "/" << version.minor;
  return (os);
}

std::ostream	&operator<<(std::ostream &os, const ZIAPI::HTTPRequest &request)
{
  os << request.getMethod() << " " << request.getURI() << " " << request.getVersion() << std::endl;
  return (os);
}
