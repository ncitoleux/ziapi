//
// HTTPVersion.hh for HTTPVersion in /home/citole_n/Epitech/httpd/API
// 
// Made by citolen
// Login   <citole_n@epitech.net>
// 
// Started on  Fri Jan  3 17:32:26 2014 citolen
// Last update Fri Jan  3 21:59:22 2014 citolen
//

#ifndef __HTTPVERSION_HH__
#define __HTTPVERSION_HH__

namespace ZIAPI
{
  ///
  /// \brief Represent HTTP version
  ///
  typedef struct	HTTPVersion
  {
    int			major;
    int			minor;
  }			HTTPVersion;
}

#endif
